import React from 'react';
import { faHeart, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './Message.css';

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isLiked: false };
    }

    likeMessageHandler(id) {
        if (this.state.isLiked) {
            document.getElementById(id).className = 'not-liked';
        } else {
            document.getElementById(id).className = 'liked';
        }
        this.setState({ ...this.state, isLiked: !this.state.isLiked });
    }

    render() {
        const { avatarUrl, text, userName } = this.props;

        let createdAt = new Date(this.props.createdAt)
            .toString()
            .split(' ')[4]
            .split(':');

        createdAt = `${createdAt[0]}:${createdAt[1]}`;

        const selectOneOfTwo = (ifTrueValue, ifFalseValue) =>
            userName === 'Yourself' ? ifTrueValue : ifFalseValue;

        const removeOrEditIconsElement = (
            <React.Fragment>
                <div
                    className='remove'
                    onClick={() => {
                        this.props.onRemove(this.props.id);
                    }}
                >
                    <FontAwesomeIcon icon={faTrash} />
                </div>
                <div
                    className='edit'
                    onClick={() => {
                        const text = prompt('ENTER NEW TEXT');
                        if (text !== null) {
                            if (text.trim() === '') {
                                alert('MESSAGE TEXT CANNOT BE EMPTY');
                            } else {
                                this.props.onEdit(this.props.id, text);
                            }
                        }
                    }}
                >
                    <FontAwesomeIcon icon={faEdit} />
                </div>
            </React.Fragment>
        );

        const likeIconElement = (
            <div
                className='not-liked'
                id={`${this.props.id}_like`}
                onClick={() => {
                    this.likeMessageHandler(`${this.props.id}_like`);
                }}
            >
                <FontAwesomeIcon icon={faHeart} />
            </div>
        );

        return (
            <li
                className={selectOneOfTwo(
                    'message right appeared',
                    'message left appeared'
                )}
                id={this.props.id}
            >
                {userName === 'Yourself' ? null : (
                    <img
                        className='avatar'
                        src={avatarUrl}
                        alt={`${userName}'s avatar`}
                        title={`${userName}`}
                    ></img>
                )}

                <div className='text_wrapper'>
                    {selectOneOfTwo(removeOrEditIconsElement, likeIconElement)}

                    <div
                        className={
                            this.props.editedAt
                                ? selectOneOfTwo(
                                      'date-yourself edited',
                                      'date edited'
                                  )
                                : selectOneOfTwo('date-yourself', 'date')
                        }
                        title={
                            this.props.editedAt
                                ? `Edited at ${new Date(
                                      this.props.editedAt
                                  ).toString()}`
                                : ''
                        }
                    >
                        {createdAt}
                    </div>
                    <div className='text'>{text}</div>
                </div>
            </li>
        );
    }
}

export default Message;
