import React from 'react';

import './MessageInput.css';

class MessageInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            inputValue: '',
        };
    }

    inputChangeHandler(event) {
        this.setState({
            ...this.state,
            inputValue: event.target.value,
        });
    }

    render() {
        return (
            <div className='bottom_wrapper clearfix'>
                <div className='message_input_wrapper'>
                    <input
                        className='message_input'
                        placeholder='Type your message here...'
                        onChange={(event) => this.inputChangeHandler(event)}
                    />
                </div>
                <div
                    className='send_message'
                    onClick={(event) => {
                        if (this.state.inputValue.trim() === '') {
                            alert('Message cannot be empty');
                        } else {
                            this.props.onclick(event, {
                                id: Math.floor(Math.random() * 10000),
                                text: this.state.inputValue,
                                user: 'Yourself',
                                avatar:
                                    'https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA',
                                userId: 'userIdHere',
                                editedAt: '',
                                createdAt: new Date().toString(),
                            });

                            const inputValue = document.getElementsByClassName(
                                'message_input'
                            )[0];
                            inputValue.value = '';

                            this.setState({
                                ...this.state,
                                inputValue: '',
                            });
                        }
                    }}
                >
                    <div className='icon'></div>
                    <div className='text'>Send</div>
                </div>
            </div>
        );
    }
}

export default MessageInput;
