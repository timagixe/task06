import React from 'react';
import Spinner from '../../components/Spinner/Spinner';
import Header from '../../components/Header/Header';
import Messages from '../../components/Messages/Messages';
import MessageInput from '../MessageInput/MessageInput';

import './Chat.css';

export class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = { messages: [], loading: true };
    }

    async fetchMessagesHandler() {
        const response = await fetch(
            'https://edikdolynskyi.github.io/react_sources/messages.json'
        );

        let responseArray = await response.json();

        this.setState({
                ...this.state,
                messages: responseArray,
                loading: false,
            });
    }

    componentDidMount() {
        setTimeout(this.fetchMessagesHandler.bind(this), 1000);
    }

    getChatParticipantsCountHandler = () => {
        let arrayOfUsers = [];

        this.state.messages.map((message) => {
            const userName = message.user;

            if (!arrayOfUsers.includes(userName)) {
                arrayOfUsers = arrayOfUsers.concat(userName);
            }
            return arrayOfUsers;
        });

        return arrayOfUsers.length;
    };

    getChatMessagesCountHandler = () => this.state.messages.length;

    getLastMessageTimeHandler = () => {
        let messages = this.state.messages;

        messages = messages.sort(function (a, b) {
            return new Date(b.createdAt) - new Date(a.createdAt);
        });

        let lastMessageWasSentAt = new Date(messages[0].createdAt)
            .toString()
            .split(' ')[4]
            .split(':');

        return lastMessageWasSentAt[0] + ':' + lastMessageWasSentAt[1];
    };

    submitButtonClickHandler(event, messageObject) {
        event.preventDefault();

        const messages = this.state.messages;

        this.setState({
            ...this.state,
            messages: messages.concat(messageObject),
        });
    }

    removeMessageHandler(id) {
        this.setState({
            ...this.setState,
            messages: this.state.messages.filter(
                (message) => message.id !== id
            ),
        });
    }

    editMessageHandler(id, text) {
        this.setState({
            ...this.setState,
            messages: this.state.messages.map((message) => {
                if (message.id === id) {
                    message.text = text;
                    message.editedAt = new Date().toString();
                }
                return message;
            }),
        });
    }

    render() {
        const submitHandler = this.submitButtonClickHandler;
        const removeHandler = this.removeMessageHandler;
        const editHandler = this.editMessageHandler;

        return (
            <div className='chat_window'>
                {this.state.loading ? (
                    <Spinner />
                ) : (
                    <React.Fragment>
                        <Header
                            chatParticipantsCount={this.getChatParticipantsCountHandler()}
                            chatMessagesCount={this.getChatMessagesCountHandler()}
                            lastMessageTime={this.getLastMessageTimeHandler()}
                        />
                        <Messages
                            messages={this.state.messages.reverse()}
                            onRemove={removeHandler.bind(this)}
                            onEdit={editHandler.bind(this)}
                        />
                        <MessageInput onclick={submitHandler.bind(this)} />
                    </React.Fragment>
                )}
            </div>
        );
    }
}
